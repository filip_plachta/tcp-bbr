//
// This file is part of an OMNeT++/OMNEST simulation example.
//
// Copyright (C) 1992-2015 Andras Varga
//
// This file is distributed WITHOUT ANY WARRANTY. See the file
// `license' for details on this and other legal matters.
//

#include "DynaPacket_m.h"
#include <algorithm>
#include <vector>

using namespace omnetpp;

#define STACKSIZE    16384

/**
 * Dynamically launched process in the server; see NED file for more info
 */
class ServerProcess : public cSimpleModule
{
  public:
    ServerProcess() : cSimpleModule(STACKSIZE) {}
    virtual void activity() override;
    int getAckId(int lastId, std::vector<int>& receivedPackets);
};

Define_Module(ServerProcess);

void ServerProcess::activity()
{
    // retrieve parameters
    cPar& processingTime = getParentModule()->par("processingTime");

    cGate *serverOutGate = getParentModule()->gate("port$o");

    int clientAddr = 0, ownAddr = 0;
    std::vector<int> receivedPackets;
    WATCH(clientAddr);
    WATCH(ownAddr);

    DynaPacket *pk;
    DynaDataPacket *datapk;

    // receive the CONN_REQ we were created to handle
    EV << "Started, waiting for DYNA_CONN_REQ\n";
    pk = (DynaPacket *)receive();
    clientAddr = pk->getSrcAddress();
    ownAddr = pk->getDestAddress();

    // set the module name to something informative
    char buf[30];
    sprintf(buf, "serverproc%d-clientaddr%d", getId(), clientAddr);
    setName(buf);

    // respond to CONN_REQ by CONN_ACK
    EV << "client is addr=" << clientAddr << ", sending DYNA_CONN_ACK\n";
    pk->setName("DYNA_CONN_ACK");
    pk->setKind(DYNA_CONN_ACK);
    pk->setSrcAddress(ownAddr);
    pk->setDestAddress(clientAddr);
    pk->setServerProcId(getId());
    sendDirect(pk, serverOutGate);

    // process data packets until DISC_REQ comes
    for ( ; ; ) {
        EV << "waiting for DATA(query) (or DYNA_DISC_REQ)\n";
        pk = (DynaPacket *)receive();
        int type = pk->getKind();

        if (type == DYNA_DISC_REQ)
            break;

        if (type != DYNA_DATA)
            throw cRuntimeError("protocol error!");
        datapk = (DynaDataPacket *)pk;
        EV << "sending DATA ACK\n";
        datapk->setName("DATA ACK");
        datapk->setKind(DYNA_DATA_ACK);
        datapk->setSrcAddress(ownAddr);
        datapk->setDestAddress(clientAddr);
        int id = getAckId(pk->getId(), receivedPackets);
        datapk->setId(id);
        sendDirect(datapk, serverOutGate);
    }

    // connection teardown in response to DISC_REQ
    EV << "got DYNA_DISC_REQ, sending DYNA_DISC_ACK\n";
    pk->setName("DYNA_DISC_ACK");
    pk->setKind(DYNA_DISC_ACK);
    pk->setSrcAddress(ownAddr);
    pk->setDestAddress(clientAddr);
    sendDirect(pk, serverOutGate);

    EV << "exiting\n";
    deleteModule();
}

int ServerProcess::getAckId(int receivedId, std::vector<int>& receivedPackets)
{
    receivedPackets.push_back(receivedId);
    std::sort(receivedPackets.begin(), receivedPackets.end(), std::less<int>());

    for (int i=0; i < receivedPackets.size()-1 ; i++)
    {
        if (receivedPackets.at(i)+1 != receivedPackets.at(i+1)) {
            // Wystepuje luka
            return receivedPackets.at(i);
        }
    }

    return receivedPackets.back();

}

