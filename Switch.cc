//
// This file is part of an OMNeT++/OMNEST simulation example.
//
// Copyright (C) 1992-2015 Andras Varga
//
// This file is distributed WITHOUT ANY WARRANTY. See the file
// `license' for details on this and other legal matters.
//

#include "DynaPacket_m.h"

using namespace omnetpp;

/**
 * Simulates a switch between clients and server; see NED file for more info
 */
class Switch : public cSimpleModule
{
  public:
    Switch() : cSimpleModule() {}
    virtual void handleMessage(cMessage* msg) override;
    virtual void initialize() override;
    cQueue queue;
    DynaPacket* selfMsg;
    int queueMaxLen;
};

Define_Module(Switch);

void Switch::initialize()
{
    queueMaxLen = (int)par("queueMaxLen");
    cQueue queue("queue");
    selfMsg = new DynaPacket();
    selfMsg->setKind(SWITCH_SEND_PACKET);
}

void Switch::handleMessage(cMessage* msg)
{

    EV << "Queue length = " << queue.getLength()<< "\n";

    // Wysylamy pakiet
    if (msg->getKind() == SWITCH_SEND_PACKET)
    {
        // Pobieramy i wysylamy najnowszy element kolejki
        msg = (cMessage *)queue.pop();
        DynaPacket *pk = check_and_cast<DynaPacket *>(msg);
        int dest = pk->getDestAddress();
        EV << "Relaying msg to addr=" << dest << "\n";
        send(msg, "port$o", dest);
        int time = exponential(1);
        if (!queue.isEmpty())
        {
            scheduleAt(simTime() + time, selfMsg);
        }

    }
    // W przeciwnym wypadku wrzucamy do kolejki
    else
    {
        // Jesli kolejka pusta -> od razu planujemy obsluge pakietu
        if (queue.isEmpty())
        {
            queue.insert(msg);
            int time = exponential(1);
            scheduleAt(simTime() + time, selfMsg);
        }
        // Jesli kolejka jest pelna to dropujemy pakiet
        else if (queue.getLength() == queueMaxLen)
        {
            EV << "Buffer overflow, discarding packet"<<endl;
            delete msg;
        }
        else
        {
            queue.insert(msg);
        }
    }
}

