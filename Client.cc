//
// This file is part of an OMNeT++/OMNEST simulation example.
//
// Copyright (C) 1992-2015 Andras Varga
//
// This file is distributed WITHOUT ANY WARRANTY. See the file
// `license' for details on this and other legal matters.
//

#include "DynaPacket_m.h"
#include <vector>
#include <cmath>

using namespace omnetpp;

// Faza TCP BBR
enum Phase{
    START_UP = 1,
    DRAIN = 2,
    PROBE_BW = 3,
    PROBE_RTT = 4
};

/**
 * Client computer; see NED file for more info
 */
class Client : public cSimpleModule
{
  public:
    Client() : cSimpleModule() {}
    virtual void initialize() override;
    virtual void handleMessage(cMessage *msg) override;
    void BBRCheckFullPipe();
  private:
    virtual DynaPacket* generatePacket();
    virtual double windowedMax();
    virtual double windowedMin();
    int lastId = 0;
    int serverProcId;
    DynaPacket* selfMsg;
    std::vector<cMessage*> ackedPackets;
    double currDeliveredPacketsSize;
    double deliveredTime;
    bool skippedPacket;
    int inflight;
    // Parametry TCP BBR
    double BtlBW;
    double RTprop;
    // Parametry zale�ne od FAZY
    double pacingGain;
    double cwndGain;
    // Obecna faza TCP_BBR
    Phase currentPhase;
    // licznik przyrostu o ponad 25 procent
    int full_bw_count;
    //licznik fullbw
    double full_bw;
    //flaga czy rura zapelniona
    bool filled_pipe;
};

Define_Module(Client);

void Client::initialize()
{
    // query module parameters
    simtime_t timeout = par("timeout");
    cPar& connectionIaTime = par("connIaTime");
    cPar& queryIaTime = par("queryIaTime");
    cPar& numQuery = par("numQuery");

    DynaPacket *connReq, *connAck, *discReq, *discAck;
    DynaDataPacket *query, *answer;
    int actNumQuery = 0, i = 0;
    WATCH(actNumQuery);
    WATCH(i);
    actNumQuery = (int) numQuery;

    // assign address: index of Switch's gate to which we are connected
    int ownAddr = gate("port$o")->getNextGate()->getIndex();
    int serverAddr = gate("port$o")->getNextGate()->size()-1;
    WATCH(ownAddr);
    WATCH(serverAddr);
    WATCH(serverProcId);
    if (hasGUI())
        getDisplayString().setTagArg("i", 1, "");

    if (hasGUI())
        getDisplayString().setTagArg("i", 1, "green");
    // connection setup
    EV << "sending DYNA_CONN_REQ\n";
    connReq = new DynaPacket("DYNA_CONN_REQ", DYNA_CONN_REQ);
    connReq->setSrcAddress(ownAddr);
    connReq->setDestAddress(serverAddr);
    send(connReq, "port$o");

    selfMsg = new DynaPacket();
    selfMsg->setKind(DYNA_SEND_PACKET);

    if (hasGUI())
        getDisplayString().setTagArg("i", 1, "blue");

    // Pocz�tkowe parametry TCP BBR (FAZA 1)
    this->cwndGain = 2 / log(2);
    this->pacingGain = 2 / log(2);

    // Ustawienie poczatkowej fazy;
    this->currentPhase = START_UP;

    currDeliveredPacketsSize = 0;
    deliveredTime = 0;
    inflight = 0;
    skippedPacket = false;
    BtlBW = 0;
    RTprop = 0;

    // wartosci poczatkowe dla obslugi zmiany na faze 2
    full_bw_count = 0;
    full_bw = 0.0;
    filled_pipe = false;

}

void Client::handleMessage(cMessage* msg)
{
    EV << " AKTUALNA FAZA: "<< this->currentPhase << endl;
    DynaPacket *pk = check_and_cast<DynaPacket *>(msg);
    // Inicjalizacja po��czenia - potwierdzenie
    if (pk->getKind() == DYNA_CONN_ACK)
    {
        EV << "DYNA_CONN_ACK received\n";
        serverProcId = pk->getServerProcId();
        EV << "got DYNA_CONN_ACK, my server process is ID="
           << serverProcId << endl;
        delete pk;

        // Wysylamy pierwszy pakiet
        scheduleAt(simTime(), selfMsg);

    }
    // Send - wysylka pakietu
    else if (msg->getKind() == DYNA_SEND_PACKET) {
        double BDP = this->BtlBW * this->RTprop;
        double CWND = 4;
        // Ignorujemy je�li nie przyszed� �aden ACK
        if (ackedPackets.size() != 0)
        {
            // Jesli mamy chociaz jednego acka to liczymy CWND
            CWND = this->cwndGain * BDP > 4 ? this->cwndGain * BDP : 4;
            if (inflight >= this->cwndGain * BDP)
            {
                skippedPacket = true;
                return;
            }
        }

        // Wysylamy pakiety w ilosci CWND [mechanizm okna]
        for (int i=0; i<(int)CWND; i++)
        {
            EV << "Sending message: "<< lastId << endl;
                   // Liczymy cwnd w celu wyznaczenia ile pakiet�w naraz wys�a�
                   DynaPacket *packet = generatePacket();

                   // Inkrementujemy ilo�� pakiet�w w locie
                   inflight++;
                   send(packet, "port$o");
        }

        // Wyliczamy za ile wysylka nastepnego pakietu
        // Jesli BtlBw == 0 -> nie mamy �adnego ACKa, wiec na niego poczekamy, zeby wyliczyc parametry
        if (this->BtlBW == 0)
        {
            skippedPacket = true;
        }
        else
        {
            int sendNextPacketTime = 1500 / this->pacingGain * this->BtlBW;
            scheduleAt(simTime() + sendNextPacketTime, selfMsg);
        }

    }

    // onAck
    // Przyszlo potwierdzenie otrzymania pakietu - przeliczamy parametry..
    else if (msg->getKind() == DYNA_DATA_ACK) {
        // sprawdzanie czy przejsc do fazy 2
        this->BBRCheckFullPipe();
        if(this->filled_pipe && this->currentPhase == START_UP){
            this->currentPhase = DRAIN;
            this->cwndGain = 2 / log(2);
            this->pacingGain = log(2)/2;
        }
        simtime_t now = simTime();
        // Dekrementujemy ilo�� pakiet�w w locie
        inflight--;
        // Ustawiamy do wyliczania RTT
        msg->setArrivalTime(now);
        ackedPackets.push_back(msg);
        EV << "Received ACK message: "<< pk->getId() << endl;
        deliveredTime = now.dbl();
        // Dostarczone Bajty
        currDeliveredPacketsSize += 1500;
        double deliveryRate_size = currDeliveredPacketsSize - pk->getCurrentDeliveredPacketsSize();
        double deliveryRate_time = now.dbl() - (pk->getDeliveredTime().dbl());
        double deliveryRate = deliveryRate_size / deliveryRate_time;

        if (deliveryRate > BtlBW || BtlBW == 0) {
            BtlBW = windowedMax();
        }
        // Liczymy parametry TCP BBR
        this->RTprop = this->windowedMin();

        if (skippedPacket)
        {
            skippedPacket = false;
            scheduleAt(simTime(), selfMsg);
        }
    }
}


/**
 * Zwraca najwieksze RTT posrod 10 ostatnich warto�ci
 */
double Client::windowedMax() {
    double max = 0;
    int size = ackedPackets.size();

    for (int i = 1; i <= 10; i++ )
    {
        if (i > size) break;

        cMessage *msg = ackedPackets.at(size - i);
        double rtt = (msg->getArrivalTime() - msg->getTimestamp()).dbl();
        if (max < rtt)
        {
            max = rtt;
        }
    }
    return max;
}

/**
 * Zwraca najmniejsze RTT z ostatnich 10 sekund
 */
double Client::windowedMin() {
    double min = 0;
    int size = ackedPackets.size();
    simtime_t current_time = simTime();

    for (int i = 0; i < size; i++ )
    {
        cMessage * msg = ackedPackets.at(i);
        // ignorujemy pakiet jesli jest starszy niz 10s
        if (current_time - msg->getArrivalTime() >= 10) continue;

        double rtt = (msg->getArrivalTime() - msg->getTimestamp()).dbl();
        if (min > rtt || min == 0)
        {
            min = rtt;
        }
    }
    return min;
}

/**
 * Generuje pakiet
 */
DynaPacket* Client::generatePacket()
{
    // Utworzenie pakietu
    DynaPacket *pk = new DynaPacket();

    // Adres serwera i klienta
    int ownAddr = gate("port$o")->getNextGate()->getIndex();
    int serverAddr = gate("port$o")->getNextGate()->size()-1;
    pk->setName("DYNA_DATA_SEND");
    pk->setKind(DYNA_DATA);
    pk->setSrcAddress(ownAddr);
    pk->setDestAddress(serverAddr);
    pk->setServerProcId(serverProcId);
    // Zapisujemy czas wyslania
    pk->setTimestamp(simTime());
    // Ustawiamy ID pakietu
    this->lastId += 1;
    pk->setId(lastId);
    // Dodatkowe parametry wykorzystywane do liczenia parametr�w BBR
    pk->setDeliveredTime(this->deliveredTime);
    pk->setCurrentDeliveredPacketsSize(currDeliveredPacketsSize);
    return pk;
}

// sprawdzanie wzrostu zapelnienia o 25 procent trzy razy pod rzad
void  Client::BBRCheckFullPipe(){
  if(this->filled_pipe || this->currentPhase != START_UP)
      return;
  if (this->BtlBW >= this->full_bw * 1.25){  // BBR.BtlBw still growing?
       this->full_bw = this->BtlBW;    // record new baseline level
       this->full_bw_count = 0;
       return;
  }
     this->full_bw_count++; // another round w/o much growth
     if (this->full_bw_count >= 3)
       this->filled_pipe = true;
}
